\documentclass{article}
\title{Python Class Tutorial}
\author{Elad Lahav {\tt e2lahav@gmail.com}}

\usepackage{graphicx}
\usepackage{framed}
\usepackage{listings}

\newenvironment{important}
{
\begin{framed}
\noindent {\bf Important!}
}
{
\end{framed}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Python syntax highlighting, courtesy of Michael Anderson:
% https://tex.stackexchange.com/questions/83882/how-to-highlight-python-syntax-in-latex-listings-lstinputlistings-command
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\lstset{basicstyle=\scriptsize,language=Python}

\usepackage[utf8]{inputenc}

% Default fixed font does not support bold face
\DeclareFixedFont{\ttb}{T1}{txtt}{bx}{n}{12} % for bold
\DeclareFixedFont{\ttm}{T1}{txtt}{m}{n}{12}  % for normal

% Custom colors
\usepackage{color}
\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.6,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}

\usepackage{listings}

% Python style for highlighting
\newcommand\pythonstyle{\lstset{
language=Python,
basicstyle=\ttm,
otherkeywords={self},             % Add keywords here
keywordstyle=\ttb\color{deepblue},
emph={MyClass,__init__},          % Custom highlighting
emphstyle=\ttb\color{deepred},    % Custom highlighting style
stringstyle=\color{deepgreen},
frame=tb,                         % Any extra options here
showstringspaces=false            %
}}


% Python environment
\lstnewenvironment{python}[1][]
{
\pythonstyle
\noindent
\begin{minipage}{\linewidth}
\lstset{#1}
\end{minipage}
}
{}

% Python for external files
\newcommand\pythonexternal[2][]{{
\pythonstyle
\lstinputlisting[#1]{#2}}}

% Python for inline
\newcommand\pythoninline[1]{{\pythonstyle\lstinline!#1!}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\maketitle

\section{Introduction}

We learned that we can use variables to hold values of different types, such as
integers, floating point numbers, strings and Booleans. Each variable has a name
and a value that is assigned to it. For example, the following code creates a
variable called $a$ with the integer value 5, and a variable called $b$ with the
string value ``hello'':

\begin{python}
  a = 5
  b = "hello"
\end{python}

Once we have a variable, we can use it in various ways, such as reading its
contents or updating the value:

\begin{python}
  a = a + 1 # Increment the value in a
  print(a)  # The print() function will read the
            # value in a
\end{python}

But what if we want to have a variable with more than one value? Here are a few
examples:

\begin{enumerate}
\item A variable that represents a pixel to draw on the screen. The pixel has a position
  on the screen, which requires two integers (one for the row and one for the
  column), and also a colour value.
\item A variable that provides contact information for a friend. The variable
  will need to store a name, an address and a phone number.
\item A variable that holds the value of a fraction, with two integers: one for
  the numerator and the other for the denominator.
\end{enumerate}

For the rest of the tutorial we will use a different example, that of a player
in a multi-player game. Every player in the game has a screen name (a string)
and a score (an integer). But what kind of thing is a player variable going to
store? It cannot be a single integer or a single string.

\section{Classes}

A {\em class} allows us to introduce a new type of value that can be stored in a
variable. Once we have added the class to our code, we will be able to create
variables that store values of a new type, in addition to numbers, strings and
Booleans.

\begin{python}
  class Player:
      name = ""
      score = 0
\end{python}

The code above adds a new type to our program. The type is {\tt Player} and it
is a combination of a name (string) and a score (integer). As you can see $name$
and $score$ look like variables inside the class. These variables are often
called the class {\em properties}.

Now we can create new variables that hold values of type {\tt Player}:

\begin{python}
  p1 = Player()
  p2 = Player()
\end{python}

The code above creates two variables, $p1$ and $p2$, each with a value of type
{\tt Player}.

\begin{important}
  The variables $p1$ and $p2$ each hold an {\em object} of type
  {\tt Player}. Every time we use the class name followed by parentheses we create a
  new object. Think of a class as a cookie cutter: every time we use it we create
  a new cookie that at first looks the same as every other cookie.
\end{important}

We can then use the dot notation to read and write the properties
of each object:

\begin{python}
  p1.name = "Adam"
  p1.score = 10
  p2.name = "Matt"
  p2.score = 5
\end{python}

Hold on! Did we not just change the name of the player from ``Adam'' to ``Matt''?
No, we didn't. The variable $p1$ has a different {\tt Player} value than that
variable $p2$. When we access a property such as $name$ by using the object name,
followed by a dot, followed by a property name, we access the property that
belongs to that object. The following code will print ``Adam vs Matt'':

\begin{python}
  print(p1.name + " vs. " + p2.name)
\end{python}

\begin{important}
  Once we have created an object it has its own version of the class
  properties. Just like our cookies: after we have cut and baked two identical
  cookies, we can take a bite from one and change its shape without changing the
  other.
\end{important}

Let's write a mini game using our {\tt Player} class:

\begin{python}
  import random

  class Player:
      score = 0
      name = ""

  p1 = Player()
  p1.name = "Adam"
  p1.score = random.randint(0, 10)

  p2 = Player()
  p2.name = "Matt"
  p2.score = random.randint(0, 10)

  print(p1.name + ": " + str(p1.score))
  print(p2.name + ": " + str(p2.score))
  if (p1.score > p2.score):
      print(p1.name + " wins!")
  else:
      print(p2.name + " wins!")
\end{python}

\subsection{What did we learn so far?}

\begin{enumerate}
  \item A {\em class} allows us to define a new type, in addition to Python's
    basic types (integers, floats, strings, Booleans).
  \item Unlike basic types, we can use the type defined by the class to hold
    multiple values of different types. These are called the class
    {\em properties}.
  \item Properties look like variables inside the class definition.
  \item We use the class name followed by parentheses to create a new
    {\em object} of the type class.
  \item Every object has its own copy of the class properties. We use
    $obj.prop$ to get or set the value of the property $prop$ in the object
    $obj$.
\end{enumerate}

\subsection{Do it yourself!}

\begin{enumerate}
\item Write a class called {\tt Fraction} with two integer properties, {\em num}
  for the numerator and {\em den} for the denominator.
\item Create two variables of type {\tt Fraction}. Have one of those store the
  fraction $2/3$ and the other store the fraction $3/5$.
\item Write a function {\tt printFraction()} that prints a fraction object. For
  example, if the variable $f$ has a nominator of 7 and a denominator of $8$
  then {\tt printFraction(f)} should print $7/8$.
\item Write a function that takes two objects of type {\tt Fraction} and
  multiplies them. The function should return a new {\tt Fraction} object that
  holds the result.
\end{enumerate}

\section{Methods}

So far we have seen classes with properties, and we have written functions that
can take objects as parameters and also return objects. Let's go back to the
example of the {\tt Player} class. Suppose that we want to write a function that
updates the player's score by a given number. We can write the function like
this:

\begin{python}
  def addScore(p, s):
      p.score = p.score + s
\end{python}

The function {\tt addScore()} has two parameters, $p$, which holds an object of
type {\tt Player} and $s$, which is an integer that is used to increment (add
to) the score of the $p$ player. We can call the function like this to add 10
points to the player object stored in the variable $p1$:

\begin{python}
  addScore(p1, 10)
\end{python}

Python allows us to do the same thing in a different way, by using class
{\em methods}. Methods are functions that belong to the class, the same way that
properties are like variables that belong to the class. To write a method, we
simply put a function inside the class, with a special first parameter called
$self$:

\begin{python}
  class Player:
      score = 0
      name = ""

      def addScore(self, s):
          self.score = self.score + s
\end{python}

To call a method for a certain class we must have an object of that class. We
use dot notation to make the call:

\begin{python}
  p1.addScore(10)
\end{python}

How does this work? When you use an object to call a method, Python puts the
object inside the special $self$ parameter. In the example above when the
{\tt addScore()} method is called by the object stored in $p1$ the same object
is put in the parameter $self$, which we then use to change the score of the
object that called the method.

Let's rewrite our mini game to use methods.

\begin{python}
  import random

  class Player:
      score = 0
      name = ""

      def setName(self, name):
          self.name = name

      def addScore(self, s):
          self.score = self.score + s

      def printPlayer(self):
          print(self.name + ": " + str(self.score))

  p1 = Player()
  p1.setName("Adam")
  p1.addScore(random.randint(0, 10))

  p2 = Player()
  p2.setName("Matt")
  p2.addScore(random.randint(0, 10))

  p1.printPlayer()
  p2.printPlayer()
  if (p1.score > p2.score):
      print(p1.name + " wins!")
  else:
      print(p2.name + " wins!")
\end{python}

What did we change here? First, we added three methods to the {\tt Player}
class:

\begin{enumerate}
\item {\tt setName()}: changes the player's name to the string in the $name$
  parameter.
\item {\tt addScore()}: increments the player's score by a given number stored
  in the $s$ parameter.
\item {\tt printPlayer()}: writes the player's name and score to the console.
\end{enumerate}

As you can see, the first parameter in each of these functions is the special
$self$.

\begin{important}
  Note that the parameter $name$ in the function {\tt setName()} is not the same
  variable as $self.name$! The first is the value passed to the method, the
  second is the property belonging to the object. If you find it confusing you
  can choose not to use parameter names that are the same as property names, but
  you should understand this point as it comes up in code.
\end{important}

We then created two {\tt Player} objects as before and stored them in the
variables $p1$ and $p2$. We used the {\tt setName()} method to give each player
a name, and we used the {\tt addScore()} method to give the player a random
score (remember that when we create a player the object's score is 0, as that is
what we assigned to the property when we defined it in the class). We used the
{\tt printPlayer()} method to print each player's name and score. The code that
decides which player wins and prints the outcome of the game is unchanged.

\begin{important}
  We did not have to give a value to the $self$ parameter when calling any of
  these methods. When we call a method with an object the object ``jumps'' into
  the $self$ parameter. So in the call {\tt p1.addScore(10)} the object in $p1$
  jumps into the $self$ parameter of the {\tt addScore()} method, pushing 10 to
  the second parameter, $s$.
\end{important}

\subsection{What did we learn so far?}

\begin{itemize}
\item A {\em method} is a function that belongs to a class.
\item We call a method by using an object, followed by dot, followed by the
  method name, with the parameter values inside parentheses.
\item Every method has a special first parameter called $self$. The value of
  $self$ is the object that was used to call the method.
\item The $self$ parameter is ignored when calling a method. The object that was
  used to call the method jumps into the $self$ parameter, pushing all values by
  one spot.
\end{itemize}

\begin{figure}[t]
  \centering
  \includegraphics[scale=0.5]{method_comic}
\end{figure}

\subsection{Do it yourself!}

\begin{itemize}
\item Turn the {\tt printFraction()} function you wrote last time into a
  method in the class {\tt Fraction()}. What parameters does the function have?
\item Write a method {\tt multiply()} that multiplies a fraction by another
  fraction. This time, instead of returning a new fraction object, the current
  fraction is updated. So if I have a fraction that represents $2/3$ in the
  variable $f1$, and a fraction that represents $4/7$ in the variable $f2$, then
  the call {\tt f1.multiply(f2)} will change the object in $f1$ to represent the
  fraction $8/21$.
\end{itemize}

\section{The {\tt \_\_init\_\_} Method}

In all the classes we've seen so far the properties were defined by assigning a
variable inside the class some value. Such values are called {\em initial}
values (``initial'' means ``first''), and giving the properties these values is
called {\em initializing}. For example, in the {\tt Player} class we initialized
the $name$ property to an empty string and the $score$ property to 0:

\begin{python}
  class Player:
      name = ""
      score = 0
\end{python}

Recall that a new object is created by calling the class as though it were a
function:

\begin{python}
  p = Player()
\end{python}

As it turns out, this code does actually call a function: it is a special
method in the class called {\tt \_\_init\_\_}. We haven't written any code for
this method so it doesn't do anything, but we can use {\tt \_\_init\_\_} to
initialize the new object's properties. For example:

\begin{python}
  class Player:
      def __init__(self):
          self.name = ""
          self.score = 0
\end{python}

As you can see, the {\tt \_\_init\_\_} method, just like any other method,
always has a $self$ parameter which holds the object that was used to make the
call. In the case of {\tt \_\_init\_\_} this is the new object that is created
by calling the class name like a function.

\begin{important}
When we have an {\tt \_\_init\_\_} method in our class we no longer need to
write the properties separately. Every variable name in the {\tt \_\_init\_\_}
method with a {\tt self.} prefix that is assigned a value becomes a property of
the object.
\end{important}

But this doesn't look very exciting - it just seems to complicate the simple way
we initialized the properties before. However, we can use {\tt \_\_init\_\_} in
more interesting ways. First, we can call other functions to give us the initial
values:

\begin{python}
  import random

  class Player:
      def __init__(self):
          self.name = ""
          self.score = random.randint(0, 10)
\end{python}

Here we gave each player a random score to begin with. Second, we can make the
{\tt \_\_init\_\_} method take more arguments:

\begin{python}
  import random

  class Player:
      def __init__(self, name):
          self.name = name
          self.score = random.randint(0, 10)
\end{python}

With this code we can create a new {\tt Player} object that immediately has the
name we want:

\begin{python}
  p = Player("Adam")
\end{python}

We can now write our game a lot more succinctly:

\begin{python}
  import random

  class Player:
      def __init__(self, name):
          self.name = name
          self.score = random.randint(0, 10)

      def printPlayer(self):
          print(self.name + ": " + str(self.score))

  p1 = Player("Adam")
  p2 = Player("Matt")

  p1.printPlayer()
  p2.printPlayer()
  if (p1.score > p2.score):
      print(p1.name + " wins!")
  else:
      print(p2.name + " wins!")
\end{python}

\subsection{What did we learn so far?}

\begin{itemize}
\item The {\tt \_\_init\_\_} method is called every time a new object is
  created, i.e., when using the name of the class like a function.
\item Like every method, {\tt \_\_init\_\_} has at least one parameter called
  $self$, which hold the new object.
\item We can use {\tt \_\_init\_\_} to give properties their starting values.
\end{itemize}

\subsection{Do it yourself!}

\begin{itemize}
\item Add an {\tt \_\_init\_\_} method to the {\tt Fraction} class. The method
  should take two parameters {\em in addition to} $self$, one for the initial
  value of the numerator and one for the initial value of the denominator.
\item Create two {\tt Fraction} objects, one for $3/4$ and one for $7/8$ using
  just one line of code for each object. Use the {\tt printFaction()} method we
  wrote last time to see that you got the right fractions.
\end{itemize}

\end{document}
