What?
-----

A short tutorial on writing and using classes in Python, geared towards young
developers.

Why?
----

My son was struggling with the concepts of classes and objects in Python. Having
experience in other object-oriented programming languages (primarily C++) I
found it hard to explain these concepts in Python. This may be my bias, or lack
of Python experience, but the combination of a terse syntax and some
idiosyncrasies seems to make these concepts more confusing, especially for
first-time programmers.

As I did not immediately find a Python class tutorial that was kid-friendly, I
decided to write one of my own.
